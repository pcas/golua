package luatesting_test

import (
	"bitbucket.org/pcas/golua/luatesting"
	"testing"
)

func TestRunLuaTest(t *testing.T) {
	src := `
print("hello, world!")
--> =hello, world!

print(1+2)
--> =3

print(1 == 1.0)
--> =true

error("hello")
--> ~!!! runtime:.*
`
	err := luatesting.RunLuaTest([]byte(src), nil)
	if err != nil {
		t.Fatal(err)
	}
}

func TestLua(t *testing.T) {
	luatesting.RunLuaTestsInDir(t, "lua", nil)
}
