module bitbucket.org/pcas/golua

go 1.13

require (
	bitbucket.org/pcastools/bytesbuffer v1.0.3
	bitbucket.org/pcastools/contextutil v1.0.3
	bitbucket.org/pcastools/stringsbuilder v1.0.3
)
