package runtime

import (
	"errors"
)

func band(t *Thread, x, y Value) (Value, error) {
	ix, okx := ToInt(x)
	iy, oky := ToInt(y)
	if okx && oky {
		return ix & iy, nil
	}
	res, err, ok := metabin(t, "__band", x, y)
	if ok {
		return res, err
	}
	return nil, errors.New("band expects bandable values")
}

func bor(t *Thread, x, y Value) (Value, error) {
	ix, okx := ToInt(x)
	iy, oky := ToInt(y)
	if okx && oky {
		return ix | iy, nil
	}
	res, err, ok := metabin(t, "__bor", x, y)
	if ok {
		return res, err
	}
	return nil, errors.New("bor expects bordable values")
}

func bxor(t *Thread, x, y Value) (Value, error) {
	ix, okx := ToInt(x)
	iy, oky := ToInt(y)
	if okx && oky {
		return ix ^ iy, nil
	}
	res, err, ok := metabin(t, "__bxor", x, y)
	if ok {
		return res, err
	}
	return nil, errors.New("bxor expects bxordable values")
}

func shl(t *Thread, x, y Value) (Value, error) {
	ix, okx := ToInt(x)
	iy, oky := ToInt(y)
	if okx && oky {
		if iy < 0 {
			return ix >> uint64(-iy), nil
		}
		return ix << uint64(iy), nil
	}
	res, err, ok := metabin(t, "__shl", x, y)
	if ok {
		return res, err
	}
	return nil, errors.New("shl expects shldable values")
}

func shr(t *Thread, x, y Value) (Value, error) {
	ix, okx := ToInt(x)
	iy, oky := ToInt(y)
	if okx && oky {
		if iy < 0 {
			return ix << uint64(iy), nil
		}
		return ix >> uint64(iy), nil
	}
	res, err, ok := metabin(t, "__shr", x, y)
	if ok {
		return res, err
	}
	return nil, errors.New("shr expects shrdable values")
}

func bnot(t *Thread, x Value) (Value, error) {
	ix, okx := ToInt(x)
	if okx {
		return ^ix, nil
	}
	res, err, ok := metaun(t, "__bnot", x)
	if ok {
		return res, err
	}
	return nil, errors.New("bnot expects a bnotable value")
}
