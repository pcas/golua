package runtime_test

import (
	"bitbucket.org/pcas/golua/luatesting"
	"testing"
)

func TestRuntime(t *testing.T) {
	luatesting.RunLuaTestsInDir(t, "lua", nil)
}
