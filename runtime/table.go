package runtime

import (
	"bitbucket.org/pcastools/bytesbuffer"
	"encoding/json"
	"errors"
)

// Table implements a Lua table.
type Table struct {

	// This table has the key-value pairs of the table.
	content map[Value]tableValue

	// This is the metatable.
	meta *Table

	// The first key in the table
	first Value

	// The border is a positive integer index n such that t[n] != nil but t[n+1]
	// == nil. If there is none such, it should be 0. It is expensive to keep
	// it up to date so it might not always be correct, in which case
	// borderState gives a clue to finding its true value.
	border Int

	// If borderState == borderOK, then border is correct. Otherwise you need
	// to look up or down depending on its value (borderCheckUp or
	// borderCheckDown).
	borderState int64
}

// NewTable returns a new Table.
func NewTable() *Table {
	return &Table{content: make(map[Value]tableValue)}
}

// Metatable returns the table's metatable.
func (t *Table) Metatable() *Table {
	return t.meta
}

// SetMetatable sets the table's metatable.
func (t *Table) SetMetatable(m *Table) {
	t.meta = m
}

// Get returns t[k].
func (t *Table) Get(k Value) Value {
	if x, ok := k.(Float); ok {
		if n, tp := x.ToInt(); tp == IsInt {
			k = n
		}
	}
	return t.content[k].value
}

// Set implements t[k] = v (doesn't check if k is nil).
func (t *Table) Set(k, v Value) {
	switch x := k.(type) {
	case Int:
		t.setInt(x, v)
		return
	case Float:
		if n, tp := x.ToInt(); tp == IsInt {
			t.setInt(n, v)
			return
		}
	}
	t.set(k, v)
}

// SetCheck implements t[k] = v, returns an error if k is nil.
func (t *Table) SetCheck(k, v Value) error {
	if IsNil(k) {
		return errors.New("table index is nil")
	}
	t.Set(k, v)
	return nil
}

// Len returns a length for t (see lua docs for details).
func (t *Table) Len() Int {
	switch t.borderState {
	case borderCheckDown:
		for t.border > 0 && t.content[t.border].value == nil {
			t.border--
		}
		t.borderState = borderOK
	case borderCheckUp:
		for t.content[t.border+1].value != nil {
			t.border++ // I don't know if that ever happens (can't test it!)
		}
		t.borderState = borderOK
	}
	return t.border
}

// Next returns the key-value pair that comes after k in the table t.
func (t *Table) Next(k Value) (next Value, val Value, ok bool) {
	var tv tableValue
	if k == nil {
		next = t.first
		ok = true
	} else {
		tv, ok = t.content[k]
		if !ok {
			return
		}
		next = tv.next
	}
	// Because we may have removed entries by setting values to nil, we loop
	// until we find a non-nil value.
	for next != nil {
		tv = t.content[next]
		if val = tv.value; val != nil {
			return
		}
		next = tv.next
	}
	return
}

// maxInt defines the maximum size of an int.
const maxInt = int64(int(^uint(0) >> 1))

// isSlice returns true iff the given table can be regarded as a slice. If true, also returns the length of the slice. Remember that Lua indexes from 1.
func isSlice(t *Table) (bool, int) {
	max := 0
	count := 0
	k, _, _ := t.Next(nil)
	for k != nil {
		if n, ok := ToInt(k); !ok || n <= 0 || int64(n) > maxInt {
			return false, 0
		} else if int(n) > max {
			max = int(n)
		}
		count++
		k, _, _ = t.Next(k)
	}
	if count != max {
		return false, 0
	}
	return true, count
}

// MarshalJSON marshals the table to JSON.
func (t *Table) MarshalJSON() ([]byte, error) {
	// Create a bytes buffer
	b := bytesbuffer.New()
	defer bytesbuffer.Reuse(b)
	// Should we treat this table as a slice or as a map?
	if ok, size := isSlice(t); ok {
		// Treat this as a slice
		b.WriteByte('[')
		for i := 1; i <= size; i++ {
			if i != 1 {
				b.WriteByte(',')
			}
			bv, err := json.Marshal(t.Get(Int(i)))
			if err != nil {
				return nil, err
			}
			b.Write(bv)
		}
		b.WriteByte(']')
	} else {
		// Treat this as a map
		b.WriteByte('{')
		first := true
		k, v, _ := t.Next(nil)
		for k != nil {
			if !first {
				b.WriteByte(',')
			}
			first = false
			s, ok := AsString(k)
			if !ok {
				return nil, errors.New("unable to convert key to string")
			}
			bk, err := json.Marshal(s)
			if err != nil {
				return nil, err
			}
			b.Write(bk)
			b.WriteByte(':')
			bv, err := json.Marshal(v)
			if err != nil {
				return nil, err
			}
			b.Write(bv)
			k, v, _ = t.Next(k)
		}
		b.WriteByte('}')
	}
	// Make a copy of the bytes and return them
	raw := b.Bytes()
	cpy := make([]byte, len(raw))
	copy(cpy, raw)
	return cpy, nil
}

const (
	borderOK int64 = iota
	borderCheckUp
	borderCheckDown
)

type tableValue struct {
	value, next Value
}

func (t *Table) setInt(n Int, v Value) {
	switch {
	case n > t.border && v != nil:
		t.border = n
		t.borderState = borderCheckUp
	case v == nil && t.border > 0 && n == t.border:
		t.border--
		t.borderState = borderCheckDown
	}
	t.set(n, v)
}

func (t *Table) set(k Value, v Value) {
	tv, ok := t.content[k]
	if v == nil && !ok {
		return
	}
	tv.value = v
	if !ok {
		tv.next = t.first
		t.first = k
	}
	t.content[k] = tv
}
