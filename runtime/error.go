package runtime

import (
	"errors"
	"fmt"
	"strings"
)

// errorContext represents an error wrapped with a context.
type errorContext struct {
	err error // The wrapped error
	c   Cont  // The context
}

// Valuer is the interface satisfied by the Value method.
type Valuer interface {
	Value() Value
}

// Contexter is the interface satisfied by the Context method.
type Contexter interface {
	Context() Cont
}

// WrapWithContext wraps the given error up as a new error with context c.
func WrapWithContext(err error, c Cont) error {
	return &errorContext{
		err: err,
		c:   c,
	}
}

// Error implements the error interface.
func (e *errorContext) Error() string {
	return fmt.Sprintf("%+v", e.err)
}

// Unwrap unwraps the error.
func (e *errorContext) Unwrap() error {
	return e.err
}

// Context returns the context for this error.
func (e *errorContext) Context() Cont {
	return e.c
}

// unwrapContext returns a slice of contexts obtained by unwrapping the given error.
func unwrapContext(err error) []Cont {
	var cs []Cont
	for err != nil {
		e, ok := err.(Contexter)
		if !ok {
			break
		}
		cs = append(cs, e.Context())
		err = errors.Unwrap(err)
	}
	return cs
}

// Traceback returns a string that represents the traceback of the error using its context.
func Traceback(err error) string {
	var tb []*DebugInfo
	for _, c := range unwrapContext(err) {
		tb = appendTraceback(tb, c)
	}
	if len(tb) == 0 {
		return err.Error()
	}
	sb := strings.Builder{}
	sb.WriteString(err.Error())
	sb.WriteByte('\n')
	for _, info := range tb {
		sourceInfo := info.Source
		if info.CurrentLine > 0 {
			sourceInfo = fmt.Sprintf("%s:%d", sourceInfo, info.CurrentLine)
		}
		sb.WriteString(fmt.Sprintf("  in function %s (file %s)\n", info.Name, sourceInfo))
	}
	return sb.String()
}
