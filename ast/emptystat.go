package ast

import (
	"bitbucket.org/pcas/golua/ir"
	"bitbucket.org/pcas/golua/token"
)

type EmptyStat struct {
	Location
}

func NewEmptyStat(tok *token.Token) EmptyStat {
	return EmptyStat{Location: LocFromToken(tok)}
}

func (s EmptyStat) HWrite(w HWriter) {
	w.Writef("empty stat")
}

func (s EmptyStat) CompileStat(c *ir.Compiler) {
	// Nothing to compile!
}
