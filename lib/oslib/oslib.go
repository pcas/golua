package oslib

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"syscall"
	"time"
)

// The maximum and minimum size of an int.
const (
	maxInt = int64(int(^uint(0) >> 1))
	minInt = -maxInt - 1
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	loc *time.Location // The location for date/time conversions
}

// Time formatting conversion (taken from the man page for strftime). If the matching token is blank, this indicates to the caller that they need to special-case this format.
var conversion = map[rune]string{
	'A': "Monday",
	'a': "Mon",
	'B': "January",
	'b': "Jan",
	'C': "2006",
	'c': "2006-01-02 15:04:05",
	'D': "1/2/06",
	'd': "02",
	'e': "2",
	'F': "2006-01-02",
	'H': "15",
	'h': "Jan",
	'I': "03",
	'j': "", // day of the year as a decimal number (1-366)
	'k': "15",
	'L': ".000",
	'l': "3",
	'M': "04",
	'm': "01",
	'n': "\n",
	'p': "PM",
	'R': "15:04",
	'r': time.Kitchen,
	'S': "05",
	's': "", // the number of seconds since the Epoch, UTC
	'T': "15:04:05",
	't': "\t",
	'U': time.UnixDate,
	'v': "2-Jan-2006",
	'V': "", // the week number of the year (1-53)
	'w': "", // the weekday as a decimal number (0-6)
	'X': "15:04:05",
	'x': "2006-01-02",
	'Y': "2006",
	'y': "06",
	'Z': "MST",
	'z': "-0700",
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setLocation sets the location for date/time operations.
func setLocation(t *rt.Thread, loc *time.Location) {
	t.Registry(registryKey).(*registryData).loc = loc
}

// getIntValue returns the value in the table t associated with the key. If no value is set, or the value is nil, returns the given default. If the value cannot be converted to an int, returns an error.
func getIntValue(t *rt.Table, key string, defaultValue int) (int, error) {
	// Get the nil table out of the way
	if t == nil {
		return defaultValue, nil
	}
	// Recover the value
	x := t.Get(rt.String(key))
	if rt.IsNil(x) {
		return defaultValue, nil
	}
	// Convert it to an integer
	n, ok := rt.ToInt(x)
	if !ok {
		return defaultValue, fmt.Errorf("'%s' must be an integer", key)
	}
	// Convert the int64 to an int
	if int64(n) < minInt || int64(n) > maxInt {
		return defaultValue, fmt.Errorf("'%s' must be in the range %d to %d (inclusive)", key, minInt, maxInt)
	}
	return int(n), nil
}

// strftime formats the time using the strftime formatting.
func strftime(format string, t time.Time) string {
	retval := make([]byte, 0, len(format))
	for i, ni := 0, 0; i < len(format); i = ni + 2 {
		ni = strings.IndexByte(format[i:], '%')
		if ni < 0 {
			ni = len(format)
		} else {
			ni += i
		}
		retval = append(retval, []byte(format[i:ni])...)
		if ni+1 < len(format) {
			c := format[ni+1]
			if c == '%' {
				retval = append(retval, '%')
			} else {
				if layoutCmd, ok := conversion[rune(c)]; ok {
					// Is this a special case?
					if len(layoutCmd) == 0 {
						switch rune(c) {
						case 'j':
							// day of the year as a decimal number (1-366)
							retval = append(retval, []byte(strconv.Itoa(t.YearDay()))...)
						case 's':
							// the number of seconds since the Epoch, UTC
							retval = append(retval, []byte(strconv.FormatInt(t.Unix(), 10))...)
						case 'V':
							// the week number of the year (1-53)
							_, m := t.ISOWeek()
							retval = append(retval, []byte(strconv.Itoa(m))...)
						case 'w':
							// the weekday as a decimal number (0-6)
							retval = append(retval, []byte(strconv.Itoa(int(t.Weekday())))...)
						}
					} else {
						retval = append(retval, []byte(t.Format(layoutCmd))...)
					}
				} else {
					retval = append(retval, '%', c)
				}
			}
		} else {
			if ni < len(format) {
				retval = append(retval, '%')
			}
		}
	}
	return string(retval)
}

// handleError pushes nil followed by a string description of the error onto next. If the error is a *os.PathError or *os.LinkError wrapping a syscall.Errno then the err number wil also be pushed onto the stack. Returns next, nil.
func handleError(err error, c *rt.GoCont) (rt.Cont, error) {
	next := c.Next()
	next.Push(nil)
	next.Push(rt.String(err.Error()))
	if e, ok := err.(*os.PathError); ok {
		if errno, ok := e.Err.(syscall.Errno); ok && errno != 0 {
			next.Push(rt.Int(errno))
		}
	} else if e, ok := err.(*os.LinkError); ok {
		if errno, ok := e.Err.(syscall.Errno); ok && errno != 0 {
			next.Push(rt.Int(errno))
		}
	}
	return next, nil
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers this module.
func init() {
	module.Register(module.Info{
		Name:        "os",
		Description: "Functionality related to the operating system.",
		URL:         "https://www.lua.org/manual/5.3/manual.html#6.9",
		Preload:     true,
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				loc: time.Local,
			})
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnvGoFunc(pkg, "clock", clock, 0, false)
			rt.SetEnvGoFunc(pkg, "date", date, 2, false)
			rt.SetEnvGoFunc(pkg, "difftime", difftime, 2, false)
			rt.SetEnvGoFunc(pkg, "getenv", getenv, 1, false)
			rt.SetEnvGoFunc(pkg, "remove", remove, 1, false)
			rt.SetEnvGoFunc(pkg, "rename", rename, 2, false)
			rt.SetEnvGoFunc(pkg, "setlocale", setlocale, 2, false)
			rt.SetEnvGoFunc(pkg, "time", timef, 1, false)
			rt.SetEnvGoFunc(pkg, "tmpname", tmpname, 0, false)
			return pkg, nil
		},
	})
}

func clock(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	var rusage syscall.Rusage
	_ = syscall.Getrusage(syscall.RUSAGE_SELF, &rusage) // ignore errors
	time := float64(rusage.Utime.Sec+rusage.Stime.Sec) + float64(rusage.Utime.Usec+rusage.Stime.Usec)/1000000.0
	return c.PushingNext(rt.Float(time)), nil
}

func date(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Is a format string provided?
	format := "%c"
	if c.NArgs() != 0 && !rt.IsNil(c.Arg(0)) {
		s, err := c.StringArg(0)
		if err != nil {
			return nil, err
		} else if len(s) == 0 {
			return nil, errors.New("#1 must be a non-empty string")
		}
		format = string(s)
	}
	// Is a time provided?
	when := time.Now()
	if c.NArgs() > 1 {
		// Note: We parse the time relative to the location set by setlocale.
		// It's unclear to me from the Lua docs whether a "!" at the beginning
		// of the format string should change this parsing to UTC.
		var err error
		if when, err = ArgToTime(t, c, 1); err != nil {
			return nil, err
		}
	}
	// Format the time
	return c.PushingNext(Date(format, when, t)), nil
}

func difftime(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	// Recover the two times
	t2, err := ArgToTime(t, c, 0)
	if err != nil {
		return nil, err
	}
	t1 := time.Now().In(Location(t))
	if c.NArgs() > 1 {
		if t1, err = ArgToTime(t, c, 1); err != nil {
			return nil, err
		}
	}
	// Return the difference
	return c.PushingNext(rt.Int(t2.Unix() - t1.Unix())), nil
}

func getenv(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	// Recover the key
	key, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	// Recover the corresponding value
	val, ok := os.LookupEnv(string(key))
	if !ok {
		return c.PushingNext(nil), nil
	}
	return c.PushingNext(rt.String(val)), nil
}

func remove(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	// Recover the path
	name, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	// Remove the file or directory
	e := os.Remove(string(name))
	if e != nil {
		return handleError(e, c)
	}
	return c.PushingNext(rt.Bool(true)), nil
}

func rename(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the paths
	oldpath, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	newpath, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	// Rename the file or directory
	e := os.Rename(string(oldpath), string(newpath))
	if e != nil {
		return handleError(e, c)
	}
	return c.PushingNext(rt.Bool(true)), nil
}

func setlocale(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Note: We check that the category is either "all" or "time". We don't
	// support any other categories, since it's unclear to me from the Lua docs
	// what the other categories mean, or what (nil, "all") should return.
	if c.NArgs() > 1 {
		if category, err := c.StringArg(1); err != nil {
			return nil, err
		} else if category != "all" && category != "time" {
			return nil, errors.New("#2 must be one of 'all' or 'time'")
		}
	}
	// Handle the case where the first argument is nil (or not set)
	if c.NArgs() == 0 || c.Arg(0) == nil {
		return c.PushingNext(rt.String(Location(t).String())), nil
	}
	// Recover the location string provided by the user
	locale, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	// Attempt to recover a location from the string
	var loc *time.Location
	if len(locale) == 0 {
		loc = time.Local
	} else if locale == "C" {
		loc = time.UTC
	} else {
		var e error
		if loc, e = time.LoadLocation(string(locale)); e != nil {
			return nil, e
		}
	}
	// Save and return the location
	setLocation(t, loc)
	return c.PushingNext(rt.String(loc.String())), nil
}

func timef(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// If no argument is provided, or the argument is nil, we return the
	// current time
	if c.NArgs() == 0 || rt.IsNil(c.Arg(0)) {
		now := time.Now().UTC().Unix()
		return c.PushingNext(rt.Int(now)), nil
	}
	// The argument must be a table
	v, err := c.TableArg(0)
	if err != nil {
		return nil, err
	}
	// Convert the table to a time
	when, err := TableToTime(t, v)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Int(when.UTC().Unix())), nil
}

func tmpname(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	f, err := ioutil.TempFile("", "lua")
	if err != nil {
		return nil, err
	}
	name := f.Name()
	if err = f.Close(); err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(name)), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Location returns the current location for date/time operations, as set by the os.setlocale function.
func Location(t *rt.Thread) *time.Location {
	return t.Registry(registryKey).(*registryData).loc
}

// Date returns a Lua value corresponding to the given time and format.
//
// Note that the formatting will take place relative to the location determined by Location, unless the first character in the format string is "!", in which case UTC is used for the location.
func Date(format string, when time.Time, t *rt.Thread) rt.Value {
	// Set the time's location
	if len(format) != 0 && format[0] == '!' {
		format = format[1:]
		when = when.In(time.UTC)
	} else {
		when = when.In(Location(t))
	}
	// Are we to return a time table?
	if format == "*t" {
		return TimeToTable(when)
	}
	// Format the time
	return rt.String(strftime(format, when))
}

// TableToTime convert the given table v to a time. The table may have fields:
//	year 				(required)
//	month (1–12) 		(required)
//	day (1–31)	 		(required)
//	hour (0–23)	 		(default: 12)
//	min (0–59)	 		(default: 0)
//	sec (0–61)	 		(default: 0)
//	millisec (0-999)	(default: 0)
//
// If any required field is missing or nil, then an error will be returned. The values for month, day, hour, min, and sec may be outside their usual ranges and will be normalised during the conversion. For example, October 32 converts to November 1.
//
// Note that the conversion will take place relative to the location determined by Location.
func TableToTime(t *rt.Thread, v *rt.Table) (time.Time, error) {
	// Check that the required keys are set
	for _, key := range []string{"year", "month", "day"} {
		if rt.IsNil(v.Get(rt.String(key))) {
			return time.Time{}, fmt.Errorf("a value for '%s' must be set", key)
		}
	}
	// Extract the values
	vals := make(map[string]int, 7)
	for _, key := range []string{"year", "month", "day", "hour", "min", "sec", "millisec"} {
		defaultValue := 0
		if key == "hour" {
			defaultValue = 12
		}
		n, err := getIntValue(v, key, defaultValue)
		if err != nil {
			return time.Time{}, err
		}
		vals[key] = n
	}
	// Convert this to a time
	return time.Date(
		vals["year"],
		time.Month(vals["month"]),
		vals["day"],
		vals["hour"],
		vals["min"],
		vals["sec"],
		vals["millisec"]*int(time.Millisecond),
		Location(t),
	), nil
}

// TimeToTable converts the given time to a table. The following fields will be set on the returned table:
//	year
//	month (1–12)
//	day (1–31)
//	hour (0–23)
//	min (0–59)
//	sec (0–61)
//	millisec (0-999)
//	wday (weekday, 1–7, Sunday is 1)
//	yday (day of the year, 1–366)
//
// Note that the conversion will take place relative to the time's location. If you need this to be relative to the location set by the user, do
//	TimeToTable(when.In(Location(t)))
// where t is the current thread.
func TimeToTable(when time.Time) *rt.Table {
	when = when.Round(time.Millisecond)
	t := rt.NewTable()
	rt.SetEnv(t, "year", rt.Int(when.Year()))
	rt.SetEnv(t, "month", rt.Int(when.Month()))
	rt.SetEnv(t, "day", rt.Int(when.Day()))
	rt.SetEnv(t, "hour", rt.Int(when.Hour()))
	rt.SetEnv(t, "min", rt.Int(when.Minute()))
	rt.SetEnv(t, "sec", rt.Int(when.Second()))
	rt.SetEnv(t, "millisec", rt.Int(when.Nanosecond()/int(time.Millisecond)))
	rt.SetEnv(t, "wday", rt.Int(when.Weekday()+1))
	rt.SetEnv(t, "yday", rt.Int(when.YearDay()))
	return t
}

// ArgToTime converts the n-th argument to a time. If the value is nil then the current time will be returned; if the value is an integer then it will be assumed to represent a UNIX time; if the value is a table then TableToTime will be used to perform the conversion.
//
// Note that the returned time will be given in the location determined by Location.
func ArgToTime(t *rt.Thread, c *rt.GoCont, n int) (time.Time, error) {
	// Recover the argument
	v := c.Arg(n)
	if rt.IsNil(v) {
		return time.Now().In(Location(t)), nil
	}
	// If the argument is an integer, then we assume it's a UNIX time
	if n, ok := rt.ToInt(v); ok {
		return time.Unix(int64(n), 0).In(Location(t)), nil
	}
	// The argument must be a table
	tt, ok := v.(*rt.Table)
	if !ok {
		return time.Time{}, fmt.Errorf("#%d must be nil, an integer, or a table", n+1)
	}
	// Convert the table to a time
	when, err := TableToTime(t, tt)
	if err != nil {
		return time.Time{}, fmt.Errorf("#%d cannot be converted to a time: %v", n+1, err)
	}
	return when, nil
}
