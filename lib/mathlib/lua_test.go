package mathlib_test

import (
	"bitbucket.org/pcas/golua/luatesting"
	"testing"
)

func TestMathLib(t *testing.T) {
	luatesting.RunLuaTestsInDir(t, "lua", nil)
}
