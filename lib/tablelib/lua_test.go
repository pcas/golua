package tablelib_test

import (
	"bitbucket.org/pcas/golua/luatesting"
	"testing"
)

func TestTable(t *testing.T) {
	luatesting.RunLuaTestsInDir(t, "lua", nil)
}
