// Package lib imports the standard Lua modules.
package lib

import (
	_ "bitbucket.org/pcas/golua/lib/coroutine"
	_ "bitbucket.org/pcas/golua/lib/debuglib"
	_ "bitbucket.org/pcas/golua/lib/iolib"
	_ "bitbucket.org/pcas/golua/lib/mathlib"
	_ "bitbucket.org/pcas/golua/lib/oslib"
	_ "bitbucket.org/pcas/golua/lib/packagelib"
	_ "bitbucket.org/pcas/golua/lib/stringlib"
	_ "bitbucket.org/pcas/golua/lib/tablelib"
	_ "bitbucket.org/pcas/golua/lib/utf8lib"
)
