package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
)

func rawlen(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	next := c.Next()
	switch x := c.Arg(0).(type) {
	case rt.String:
		next.Push(rt.Int(len(x)))
		return next, nil
	case *rt.Table:
		next.Push(x.Len())
		return next, nil
	}
	return nil, errors.New("#1 must be a string or table")
}
