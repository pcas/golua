package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
)

func rawequal(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	res, _ := rt.RawEqual(c.Arg(0), c.Arg(1))
	c.Next().Push(rt.Bool(res))
	return c.Next(), nil
}
