package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
)

func errorF(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	next := c.Next()
	if c.NArgs() >= 2 {
		level, err := c.IntArg(1)
		if err != nil {
			return nil, err
		}
		if level < 1 {
			return nil, errors.New("#2 must be > 0")
		}
		for level > 1 && next != nil {
			next = next.Next()
			level--
		}
	}
	s, err := ToString(t, c.Arg(0))
	if err != nil {
		return nil, err
	}
	return nil, errors.New(string(s)) // rt.NewError(c.Arg(0)).AddContext(next)
}
