package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
)

func pcall(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	next := c.Next()
	res := rt.NewTerminationWith(0, true)
	if err := rt.Call(t, c.Arg(0), c.Etc(), res); err != nil {
		next.Push(rt.Bool(false))
		if e, ok := err.(rt.Valuer); ok {
			next.Push(e.Value())
		} else {
			next.Push(rt.String(err.Error()))
		}
	} else {
		next.Push(rt.Bool(true))
		rt.Push(next, res.Etc()...)
	}
	return next, nil
}
