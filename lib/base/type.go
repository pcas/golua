package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
)

func typeString(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	c.Next().Push(rt.Type(c.Arg(0)))
	return c.Next(), nil
}
