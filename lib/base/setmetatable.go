package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
)

func setmetatable(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	tbl, err := c.TableArg(0)
	if err != nil {
		return nil, err
	}
	if rt.RawGet(tbl.Metatable(), rt.String("__metatable")) != nil {
		return nil, errors.New("cannot set metatable")
	}
	if rt.IsNil(c.Arg(1)) {
		tbl.SetMetatable(nil)
	} else if meta, err := c.TableArg(1); err == nil {
		tbl.SetMetatable(meta)
	} else {
		return nil, err
	}
	return c.PushingNext(c.Arg(0)), nil
}
