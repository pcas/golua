package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
)

func dofile(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	chunk, chunkName, err := loadChunk(c.Args())
	if err != nil {
		return nil, err
	}
	clos, err := rt.CompileAndLoadLuaChunk(chunkName, chunk, t.GlobalEnv())
	if err != nil {
		return nil, err
	}
	return rt.ContWithArgs(clos, nil, c.Next()), nil
}
