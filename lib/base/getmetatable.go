package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
)

func getmetatable(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	return c.PushingNext(t.Metatable(c.Arg(0))), nil
}
