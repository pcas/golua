package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
)

func pairs(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	coll := c.Arg(0)
	next := c.Next()
	res := rt.NewTerminationWith(0, true)
	err, ok := rt.Metacall(t, coll, "__pairs", []rt.Value{coll}, res)
	if ok {
		if err != nil {
			return nil, err
		}
		rt.Push(next, res.Etc()...)
		return next, nil
	}
	rt.Push(next, nextGoFunc, coll, nil)
	return next, nil
}
