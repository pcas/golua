package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
)

func rawset(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(3); err != nil {
		return nil, err
	}
	tbl, err := c.TableArg(0)
	if err != nil {
		return nil, err
	}
	key := c.Arg(1)
	if rt.IsNil(key) {
		return nil, errors.New("#2 must not be nil")
	}
	if err := tbl.SetCheck(key, c.Arg(2)); err != nil {
		return nil, err
	}
	return c.PushingNext(c.Arg(0)), nil
}
