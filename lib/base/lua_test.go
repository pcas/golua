package base_test

import (
	"bitbucket.org/pcas/golua/luatesting"
	"testing"
)

func TestBaseLib(t *testing.T) {
	luatesting.RunLuaTestsInDir(t, "lua", nil)
}
