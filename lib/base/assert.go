package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"fmt"
)

// errorValue represents a Value as an error.
type errorValue struct {
	v rt.Value // The value
	c rt.Cont  // The context
}

// Error returns the error message.
func (e *errorValue) Error() string {
	return fmt.Sprintf("%+v", e.v)
}

// Value returns the message of the error (which can be any Lua Value).
func (e *errorValue) Value() rt.Value {
	return e.v
}

// Context returns the Lua context for this error.
func (e *errorValue) Context() rt.Cont {
	return e.c
}

func assert(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	arg := c.Arg(0)
	etc := c.Etc()
	if !rt.Truth(arg) {
		if len(etc) == 0 {
			return nil, errors.New("assertion failed!")
		}
		return nil, &errorValue{
			v: etc[0],
			c: c,
		}
	}
	next := c.Next()
	next.Push(arg)
	rt.Push(next, etc...)
	return next, nil
}
