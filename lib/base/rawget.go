package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
)

func rawget(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	tbl, err := c.TableArg(0)
	if err != nil {
		return nil, err
	}
	c.Next().Push(rt.RawGet(tbl, c.Arg(1)))
	return c.Next(), nil
}
