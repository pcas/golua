package base

import (
	rt "bitbucket.org/pcas/golua/runtime"
)

func loadfile(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	chunk, chunkName, err := loadChunk(c.Args())
	if err != nil {
		return nil, err
	}
	var chunkMode string
	var chunkEnv = t.GlobalEnv()
	switch nargs := c.NArgs(); {
	case nargs >= 3:
		var err error
		chunkEnv, err = c.TableArg(2)
		if err != nil {
			return nil, err
		}
		fallthrough
	case nargs >= 2:
		mode, err := c.StringArg(1)
		if err != nil {
			return nil, err
		}
		chunkMode = string(mode)
	}
	// TODO: use mode
	_ = chunkMode
	clos, err := rt.CompileAndLoadLuaChunk(chunkName, chunk, chunkEnv)
	if err != nil {
		return nil, err
	}
	c.Next().Push(clos)
	return c.Next(), nil
}
