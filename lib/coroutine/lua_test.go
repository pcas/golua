package coroutine_test

import (
	"bitbucket.org/pcas/golua/luatesting"
	"testing"
)

func TestCoroutineLib(t *testing.T) {
	luatesting.RunLuaTestsInDir(t, "lua", nil)
}
