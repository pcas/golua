package coroutine

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
)

// init registers this module.
func init() {
	module.Register(module.Info{
		Name:        "coroutine",
		Description: "Collaborative multithreading functions.",
		URL:         "https://www.lua.org/manual/5.3/manual.html#6.2",
		Preload:     true,
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			pkg := rt.NewTable()
			rt.SetEnvGoFunc(pkg, "create", create, 1, false)
			rt.SetEnvGoFunc(pkg, "isyieldable", isyieldable, 0, false)
			rt.SetEnvGoFunc(pkg, "resume", resume, 1, true)
			rt.SetEnvGoFunc(pkg, "running", running, 0, false)
			rt.SetEnvGoFunc(pkg, "status", status, 1, false)
			rt.SetEnvGoFunc(pkg, "wrap", wrap, 1, false)
			rt.SetEnvGoFunc(pkg, "yield", yield, 0, true)
			return pkg, nil
		},
	})
}

func create(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	var f rt.Callable
	err := c.Check1Arg()
	if err == nil {
		f, err = c.CallableArg(0)
	}
	if err != nil {
		return nil, err
	}
	co := rt.NewThread(t.Runtime)
	co.Start(f)
	c.Next().Push(co)
	return c.Next(), nil
}

func resume(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	co, err := c.ThreadArg(0)
	if err != nil {
		return nil, err
	}
	next := c.Next()
	if res, err := co.Resume(t, c.Etc()); err == nil {
		next.Push(rt.Bool(true))
		rt.Push(next, res...)
	} else {
		next.Push(rt.Bool(false))
		if e, ok := err.(rt.Valuer); ok {
			next.Push(e.Value())
		} else {
			next.Push(rt.String(err.Error()))
		}
	}
	return next, nil
}

func yield(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	res, err := t.Yield(c.Etc())
	if err != nil {
		return nil, err
	}
	rt.Push(c.Next(), res...)
	return c.Next(), nil
}

func isyieldable(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	next := c.Next()
	next.Push(rt.Bool(!t.IsMain()))
	return next, nil
}

func status(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	var co *rt.Thread
	err := c.Check1Arg()
	if err == nil {
		co, err = c.ThreadArg(0)
	}
	if err != nil {
		return nil, err
	}
	var status string
	if co == t {
		status = "running"
	} else {
		switch co.Status() {
		case rt.ThreadDead:
			status = "dead"
		case rt.ThreadSuspended:
			status = "suspended"
		case rt.ThreadOK:
			status = "normal"
		}
	}
	next := c.Next()
	next.Push(rt.String(status))
	return next, nil
}

func running(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	next := c.Next()
	next.Push(t)
	next.Push(rt.Bool(t.IsMain()))
	return next, nil
}

func wrap(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	var f rt.Callable
	err := c.Check1Arg()
	if err == nil {
		f, err = c.CallableArg(0)
	}
	if err != nil {
		return nil, err
	}
	co := rt.NewThread(t.Runtime)
	co.Start(f)
	w := rt.NewGoFunction(func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		res, err := co.Resume(t, c.Etc())
		if err != nil {
			return nil, err
		}
		rt.Push(c.Next(), res...)
		return c.Next(), nil
	}, "wrap", 0, true)
	next := c.Next()
	next.Push(w)
	return next, nil
}
