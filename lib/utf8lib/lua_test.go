package utf8lib_test

import (
	"bitbucket.org/pcas/golua/luatesting"
	"testing"
)

func TestUtf8Lib(t *testing.T) {
	luatesting.RunLuaTestsInDir(t, "lua", nil)
}
