package iolib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"runtime"
	"strings"
)

// File embeds an os.File and provides an IsClosed method.
type File struct {
	*os.File
	deleteOnClose bool  // Should we delete the file on close?
	isClosed      bool  // Is the file closed?
	closeErr      error // The error on close (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// fileMethod wraps the GoFunction f with a check that arg(0) is a *File.
func fileMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := FileArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

/////////////////////////////////////////////////////////////////////////
// File functions
/////////////////////////////////////////////////////////////////////////

// TempFile makes a temporary file, which will be removed when closed. A finaliser is set on the returned file (via runtime.SetFinalizer) so that it is automatically closed when garbage collected.
func TempFile() (*File, error) {
	fh, err := ioutil.TempFile("", "lua")
	if err != nil {
		return nil, err
	}
	f := &File{
		File:          fh,
		deleteOnClose: true,
	}
	runtime.SetFinalizer(f, func(f *File) { f.Close() })
	return f, nil
}

// OpenFile opens a file with the given name in the given Lua mode. A finaliser is set on the returned file (via runtime.SetFinalizer) so that it is automatically closed when garbage collected.
func OpenFile(name string, mode string) (*File, error) {
	var flag int
	switch strings.TrimSuffix(mode, "b") {
	case "r":
		flag = os.O_RDONLY
	case "w":
		flag = os.O_WRONLY | os.O_CREATE | os.O_TRUNC
	case "a":
		flag = os.O_WRONLY | os.O_CREATE | os.O_APPEND
	case "r+":
		flag = os.O_RDWR
	case "w+":
		flag = os.O_RDWR | os.O_CREATE | os.O_TRUNC
	case "a+":
		flag = os.O_RDWR | os.O_CREATE | os.O_APPEND
	default:
		return nil, errors.New("invalid mode")
	}
	fh, err := os.OpenFile(name, flag, 0666)
	if err != nil {
		return nil, err
	}
	f := &File{File: fh}
	runtime.SetFinalizer(f, func(f *File) { f.Close() })
	return f, nil
}

// Close closes the file.
func (f *File) Close() error {
	if f == nil {
		return nil
	} else if !f.isClosed {
		f.closeErr = f.File.Close()
		f.isClosed = true
		if f.deleteOnClose {
			os.Remove(f.Name())
		}
		runtime.SetFinalizer(f, nil)
	}
	return f.closeErr
}

// IsClosed returns true iff the file is known to be closed.
func (f *File) IsClosed() bool {
	return f == nil || f.isClosed
}

// String returns a string description of the file.
func (f *File) String() string {
	if f == nil {
		return "file(nil)"
	}
	return "file(\"" + f.Name() + "\")"
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createFileMetatable creates the metatable for a file.
func createFileMetatable() *rt.Table {
	// Create the methods for a file
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "read", fileMethod(iobindings.Read), 1, true)
	rt.SetEnvGoFunc(methods, "lines", fileMethod(iobindings.Lines), 1, true)
	rt.SetEnvGoFunc(methods, "close", fileMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", fileMethod(iobindings.IsClosed), 1, false)
	rt.SetEnvGoFunc(methods, "flush", fileMethod(iobindings.Flush), 1, false)
	rt.SetEnvGoFunc(methods, "seek", fileMethod(iobindings.Seek), 3, false)
	// TODO: setvbuf
	rt.SetEnvGoFunc(methods, "write", fileMethod(iobindings.Write), 1, true)
	// Create the metatable for a file
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", fileMethod(iobindings.ToString), 1, false)
	return meta
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// FileArg turns a continuation argument into a *File.
func FileArg(c *rt.GoCont, n int) (*File, error) {
	f, ok := ValueToFile(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a file", n+1)
	}
	return f, nil
}

// ValueToFile turns a Lua value to a *File if possible.
func ValueToFile(v rt.Value) (*File, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	f, ok := u.Value().(*File)
	return f, ok
}
