package iolib_test

import (
	"bitbucket.org/pcas/golua/luatesting"
	"testing"
)

func TestIoLib(t *testing.T) {
	luatesting.RunLuaTestsInDir(t, "lua", nil)
}
