package iolib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"io"
	"os"
)

type ioKeyType struct{}

var ioKey = ioKeyType{}

type ioData struct {
	defaultOutput rt.Value
	defaultInput  rt.Value
	metatable     *rt.Table
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// errFileOrFilename returns a common error.
func errFileOrFilename() error {
	return errors.New("#1 must be a file or a filename")
}

/////////////////////////////////////////////////////////////////////////
// ioData functions
/////////////////////////////////////////////////////////////////////////

func getIoData(t *rt.Thread) *ioData {
	return t.Registry(ioKey).(*ioData)
}

func (d *ioData) defaultOutputFile() *File {
	f, _ := ValueToFile(d.defaultOutput)
	return f
}

func (d *ioData) defaultInputFile() *File {
	f, _ := ValueToFile(d.defaultInput)
	return f
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers this module.
func init() {
	module.Register(module.Info{
		Name:        "io",
		Description: "I/O operations on files.",
		URL:         "https://www.lua.org/manual/5.3/manual.html#6.8",
		Preload:     true,
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			meta := createFileMetatable()
			stdin := rt.NewUserData(&File{File: os.Stdout}, meta)
			stdout := rt.NewUserData(&File{File: os.Stdin}, meta)
			stderr := rt.NewUserData(&File{File: os.Stderr}, meta)
			r.SetRegistry(ioKey, &ioData{
				defaultOutput: stdin,
				defaultInput:  stdout,
				metatable:     meta,
			})
			pkg := rt.NewTable()
			rt.SetEnv(pkg, "stdin", stdin)
			rt.SetEnv(pkg, "stdout", stdout)
			rt.SetEnv(pkg, "stderr", stderr)
			rt.SetEnvGoFunc(pkg, "close", ioclose, 1, false)
			rt.SetEnvGoFunc(pkg, "flush", ioflush, 0, false)
			rt.SetEnvGoFunc(pkg, "input", input, 1, false)
			rt.SetEnvGoFunc(pkg, "lines", iolines, 1, true)
			rt.SetEnvGoFunc(pkg, "open", open, 2, false)
			rt.SetEnvGoFunc(pkg, "output", output, 1, false)
			// TODO: popen
			rt.SetEnvGoFunc(pkg, "read", ioread, 0, true)
			rt.SetEnvGoFunc(pkg, "tmpfile", tmpfile, 0, false)
			rt.SetEnvGoFunc(pkg, "type", typef, 1, false)
			rt.SetEnvGoFunc(pkg, "write", iowrite, 0, true)

			return pkg, nil
		},
	})
}

func ioclose(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if c.NArgs() == 0 {
		return iobindings.CloseWithCloser(getIoData(t).defaultOutputFile(), c)
	}
	return fileMethod(iobindings.Close)(t, c)
}

func ioflush(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if c.NArgs() == 0 {
		return iobindings.SyncWithSyncer(getIoData(t).defaultOutputFile(), c)
	}
	return fileMethod(iobindings.Flush)(t, c)
}

func iowrite(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.WriteWithWriter(getIoData(t).defaultOutput, c)
}

func ioread(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.ReadWithReader(getIoData(t).defaultInputFile(), t, c)
}

func iolines(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	var r io.Reader
	if c.NArgs() == 0 {
		r = getIoData(t).defaultInputFile()
	} else {
		name, err := c.StringArg(0)
		if err != nil {
			return nil, err
		}
		var ioErr error
		if r, ioErr = OpenFile(string(name), "r"); ioErr != nil {
			return nil, ioErr
		}
	}
	return iobindings.LinesWithReader(r, c, true)
}

func input(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	ioData := getIoData(t)
	if c.NArgs() == 0 {
		return c.PushingNext(ioData.defaultInput), nil
	}
	var fv rt.Value
	v := c.Arg(0)
	if s, ok := v.(rt.String); ok {
		f, err := OpenFile(string(s), "r")
		if err != nil {
			return nil, err
		}
		fv = rt.NewUserData(f, ioData.metatable)
	} else if _, ok := ValueToFile(v); ok {
		fv = v
	} else {
		return nil, errFileOrFilename()
	}
	ioData.defaultInput = fv
	return c.Next(), nil
}

func output(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	ioData := getIoData(t)
	if c.NArgs() == 0 {
		return c.PushingNext(ioData.defaultOutput), nil
	}
	var fv rt.Value
	v := c.Arg(0)
	if s, ok := v.(rt.String); ok {
		f, err := OpenFile(string(s), "w")
		if err != nil {
			return nil, err
		}
		fv = rt.NewUserData(f, ioData.metatable)
	} else if _, ok := ValueToFile(v); ok {
		fv = v
	} else {
		return nil, errFileOrFilename()
	}
	getIoData(t).defaultOutput = fv
	return c.Next(), nil
}

func open(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	fname, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	mode := rt.String("r")
	if c.NArgs() >= 2 {
		mode, err = c.StringArg(1)
		if err != nil {
			return nil, err
		}
	}
	f, ioErr := OpenFile(string(fname), string(mode))
	if ioErr != nil {
		return nil, ioErr
	}
	return c.PushingNext(rt.NewUserData(f, getIoData(t).metatable)), nil
}

func typef(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	f, err := FileArg(c, 0)
	var val rt.Value
	if err != nil {
		val = nil
	} else if f.IsClosed() {
		val = rt.String("closed file")
	} else {
		val = rt.String("file")
	}
	return c.PushingNext(val), nil
}

func tmpfile(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	f, err := TempFile()
	if err != nil {
		return nil, err
	}
	fv := rt.NewUserData(f, getIoData(t).metatable)
	return c.PushingNext(fv), nil
}
