package stringlib

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcastools/stringsbuilder"
)

func dump(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	cl, err := c.ClosureArg(0)
	if err != nil {
		return nil, err
	}
	strip := false
	if c.NArgs() >= 2 {
		strip = rt.Truth(c.Arg(1))
	}
	// TODO: support strip
	_ = strip
	w := stringsbuilder.New()
	defer stringsbuilder.Reuse(w)
	if err := rt.WriteConst(w, cl.Code.RefactorConsts()); err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(w.String())), nil
}
