package stringlib

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"fmt"
	"strings"
)

// init registers this module.
func init() {
	module.Register(module.Info{
		Name:        "string",
		Description: "String manipulation functions.",
		URL:         "https://www.lua.org/manual/5.3/manual.html#6.4",
		Preload:     true,
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			pkg := rt.NewTable()
			rt.SetEnvGoFunc(pkg, "byte", bytef, 3, false)
			rt.SetEnvGoFunc(pkg, "char", char, 0, true)
			rt.SetEnvGoFunc(pkg, "dump", dump, 2, false)
			rt.SetEnvGoFunc(pkg, "find", find, 4, false)
			rt.SetEnvGoFunc(pkg, "gmatch", gmatch, 2, false)
			rt.SetEnvGoFunc(pkg, "gsub", gsub, 4, false)
			rt.SetEnvGoFunc(pkg, "len", lenf, 1, false)
			rt.SetEnvGoFunc(pkg, "lower", lower, 1, false)
			rt.SetEnvGoFunc(pkg, "match", match, 3, false)
			rt.SetEnvGoFunc(pkg, "upper", upper, 1, false)
			rt.SetEnvGoFunc(pkg, "rep", rep, 3, false)
			rt.SetEnvGoFunc(pkg, "reverse", reverse, 1, false)
			rt.SetEnvGoFunc(pkg, "sub", sub, 3, false)
			rt.SetEnvGoFunc(pkg, "format", format, 1, true)
			rt.SetEnvGoFunc(pkg, "pack", pack, 1, true)
			rt.SetEnvGoFunc(pkg, "packsize", packsize, 1, false)
			rt.SetEnvGoFunc(pkg, "unpack", unpack, 3, false)

			stringMeta := rt.NewTable()
			rt.SetEnv(stringMeta, "__index", pkg)
			r.SetStringMeta(stringMeta)

			return pkg, nil
		},
	})
}

func maxpos(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func minpos(i, j int) int {
	if i < j {
		return i
	}
	return j
}

func bytef(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	i, j := 1, 1
	if c.NArgs() >= 2 {
		ii, err := c.IntArg(1)
		if err != nil {
			return nil, err
		}
		i = s.NormPos(ii)
		j = i
	}
	if c.NArgs() >= 3 {
		jj, err := c.IntArg(2)
		if err != nil {
			return nil, err
		}
		j = s.NormPos(jj)
	}
	next := c.Next()
	i = maxpos(1, i)
	j = minpos(len(s), j)
	for i <= j {
		next.Push(rt.Int(s[i-1]))
		i++
	}
	return next, nil
}

func char(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	vals := c.Etc()
	buf := make([]byte, len(vals))
	for i, v := range vals {
		x, ok := rt.ToInt(v)
		if !ok {
			return nil, errors.New("arguments must be integers")
		}
		if x < 0 || x > 255 {
			return nil, fmt.Errorf("#%d out of range", i+1)
		}
		buf[i] = byte(x)
	}
	return c.PushingNext(rt.String(buf)), nil
}

func lenf(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Int(len(s))), nil
}

func lower(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	s = rt.String(strings.ToLower(string(s)))
	return c.PushingNext(s), nil
}

func upper(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	s = rt.String(strings.ToUpper(string(s)))
	return c.PushingNext(s), nil
}

func rep(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	ls, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	ln, err := c.IntArg(1)
	if err != nil {
		return nil, err
	}
	n := int(ln)
	if n < 0 {
		return nil, errors.New("#2 out of range")
	}
	var sep []byte
	if c.NArgs() >= 3 {
		lsep, err := c.StringArg(2)
		if err != nil {
			return nil, err
		}
		sep = []byte(lsep)
	}
	if n == 0 {
		return c.PushingNext(rt.String("")), nil
	}
	if n == 1 {
		return c.PushingNext(ls), nil
	}
	if sep == nil {
		if len(ls)*n/n != len(ls) {
			// Overflow
			return nil, errors.New("rep causes overflow")
		}
		return c.PushingNext(rt.String(strings.Repeat(string(ls), n))), nil
	}
	s := []byte(ls)
	builder := strings.Builder{}
	sz1 := n * len(s)
	sz2 := (n - 1) * len(sep)
	sz := sz1 + sz2
	if sz1/n != len(s) || sz2/(n-1) != len(sep) || sz < 0 {
		return nil, errors.New("rep causes overflow")
	}
	builder.Grow(sz)
	builder.Write(s)
	for {
		n--
		if n == 0 {
			break
		}
		builder.Write(sep)
		builder.Write(s)
	}
	return c.PushingNext(rt.String(builder.String())), nil
}

func reverse(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	sb := []byte(s)
	l := len(s) - 1
	for i := 0; 2*i <= l; i++ {
		sb[i], sb[l-i] = sb[l-i], sb[i]
	}
	return c.PushingNext(rt.String(sb)), nil
}

func sub(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	ii, err := c.IntArg(1)
	if err != nil {
		return nil, err
	}
	i := s.NormPos(ii)
	j := len(s)
	if c.NArgs() >= 3 {
		jj, err := c.IntArg(2)
		if err != nil {
			return nil, err
		}
		j = s.NormPos(jj)
	}
	var slice rt.String
	i = maxpos(1, i)
	j = minpos(len(s), j)
	if i <= len(s) && i <= j {
		slice = s[i-1 : j]
	}
	return c.PushingNext(slice), nil
}
