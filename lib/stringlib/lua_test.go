package stringlib_test

import (
	"bitbucket.org/pcas/golua/luatesting"
	"testing"
)

func TestStringLib(t *testing.T) {
	luatesting.RunLuaTestsInDir(t, "lua", nil)
}
