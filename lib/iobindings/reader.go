// Reader contains reusable Lua bindings for an io.Reader.

package iobindings

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcastools/stringsbuilder"
	"context"
	"errors"
	"fmt"
	"io"
)

// Common errors.
var (
	errInvalidFormat    = errors.New("invalid format")
	errFormatOutOfRange = errors.New("format out of range")
)

// formatReader reads from the given reader, returning a Lua value.
type formatReader func(io.Reader, *rt.Thread) (rt.Value, error)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createByteReader returns a formatReader that returns a Lua string made of up to n bytes from r.
func createByteReader(n int) formatReader {
	b := make([]byte, n)
	return func(r io.Reader, t *rt.Thread) (rt.Value, error) {
		var n int
		var err error
		select {
		case <-t.Interrupt():
			err = context.Canceled
		default:
			n, err = r.Read(b)
		}
		if err == nil || err == io.EOF && n > 0 {
			return rt.String(b[:n]), nil
		}
		return nil, err
	}
}

// createNumberReader TODO
func createNumberReader() formatReader {
	return func(_ io.Reader, _ *rt.Thread) (rt.Value, error) {
		return nil, errors.New("not implemented") // TODO
	}
}

// createLineReader returns a formatReader that reads a line from r. If withEnd is true, it will include the end of the line in the returned value.
func createLineReader(withEnd bool) formatReader {
	b := make([]byte, 1)
	return func(r io.Reader, t *rt.Thread) (rt.Value, error) {
		// Make a note of the interrupt channel
		doneC := t.Interrupt()
		// Fetch a string builder
		buf := stringsbuilder.New()
		defer stringsbuilder.Reuse(buf)
		// We need to read a byte at a time looking for a new line
		for {
			var err error
			select {
			case <-doneC:
				err = context.Canceled
			default:
				_, err = r.Read(b)
			}
			if err != nil {
				if err != io.EOF || buf.Len() == 0 {
					return nil, err
				}
				break
			}
			end := b[0] == '\n'
			if withEnd || !end {
				buf.Write(b)
			}
			if end {
				break
			}
		}
		// Return the string
		return rt.String(buf.String()), nil
	}
}

// createReadAll reads a formatReader that reads the remaining content from r, returning it as a Lua string.
func createReadAll() formatReader {
	return func(r io.Reader, t *rt.Thread) (rt.Value, error) {
		s, err := ReadAll(r, t)
		if err != nil {
			return nil, err
		}
		return rt.String(s), nil
	}
}

// createFormatReader returns a formatReader using the indicated format:
//	n		read n bytes as a string;
//	'n'		read a number;
//	'a'		read all remaining data as a string;
//	'l'		read the next line, as a string, not including terminating '\n';
//	'L'		read the next line, as a string, including terminating '\n'.
func createFormatReader(fmt rt.Value) (reader formatReader, err error) {
	if n, ok := rt.ToInt(fmt); ok {
		if n < 0 {
			return nil, errFormatOutOfRange
		}
		reader = createByteReader(int(n))
	} else if s, ok := fmt.(rt.String); ok && len(s) > 0 {
		switch s[0] {
		case 'n':
			reader = createNumberReader()
		case 'a':
			reader = createReadAll()
		case 'l':
			reader = createLineReader(false)
		case 'L':
			reader = createLineReader(true)
		default:
			return nil, errInvalidFormat
		}
	} else {
		return nil, errInvalidFormat
	}
	return
}

// createFormatReaders returns a slice of formatReaders corresponding to the given slice of formats.
func createFormatReaders(fmts []rt.Value) ([]formatReader, error) {
	readers := make([]formatReader, 0, len(fmts))
	for _, fmt := range fmts {
		reader, err := createFormatReader(fmt)
		if err != nil {
			return nil, err
		}
		readers = append(readers, reader)
	}
	return readers, nil
}

// readUsingFormatReaders reads from r using the given slice of format readers. If no format readers are provided, then a line reader is used. The results are pushed onto the continuation.
func readUsingFormatReaders(r io.Reader, readers []formatReader, t *rt.Thread, next rt.Cont) error {
	// If necessary, use the default format reader
	if len(readers) == 0 {
		readers = []formatReader{createLineReader(false)}
	}
	// Start reading
	for _, reader := range readers {
		val, err := reader(r, t)
		next.Push(val)
		if err != nil {
			if err == context.Canceled {
				err = errors.New("user interrupt")
			}
			return err
		}
	}
	return nil
}

// linesUsingFormatReaders TODO
func linesUsingFormatReaders(r io.Reader, readers []formatReader, closeAtEOF bool) *rt.GoFunction {
	// If necessary, use the default format reader
	if len(readers) == 0 {
		readers = []formatReader{createLineReader(false)}
	}
	// Create the iterator function
	iterator := func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		next := c.Next()
		err := readUsingFormatReaders(r, readers, t, next)
		if err != nil {
			if err == io.EOF && closeAtEOF {
				if cl, ok := r.(io.Closer); ok {
					if closeErr := cl.Close(); closeErr != nil {
						err = closeErr
					}
				}
			}
			if err != io.EOF {
				return nil, err
			}
		}
		return next, nil
	}
	// Return the iterator function
	return rt.NewGoFunction(iterator, "linesiterator", 0, false)
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// Read TODO
func Read(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	r, err := ReaderArg(c, 0)
	if err != nil {
		return nil, err
	}
	return ReadWithReader(r, t, c)
}

// ReadWithReader TODO
func ReadWithReader(r io.Reader, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	readers, err := createFormatReaders(c.Etc())
	if err != nil {
		return nil, err
	}
	next := c.Next()
	err = readUsingFormatReaders(r, readers, t, next)
	if err != nil && err != io.EOF {
		return nil, err
	}
	return next, nil
}

// Lines TODO
func Lines(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	r, err := ReaderArg(c, 0)
	if err != nil {
		return nil, err
	}
	return LinesWithReader(r, c, false)
}

// LinesWithReader TODO
func LinesWithReader(r io.Reader, c *rt.GoCont, closeAtEOF bool) (rt.Cont, error) {
	readers, err := createFormatReaders(c.Etc())
	if err != nil {
		return nil, err
	}
	return c.PushingNext(linesUsingFormatReaders(r, readers, closeAtEOF)), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ReadAll reads from r until an error, EOF, or the interrupt channel on t fires, and returns the data it read. A successful call returns err == nil, not err == EOF. Because ReadAll is defined to read from src until EOF, it does not treat an EOF from Read as an error to be reported.
func ReadAll(r io.Reader, t *rt.Thread) (string, error) {
	// Make a note of the interrupt channel
	doneC := t.Interrupt()
	// Create a bytes slice
	b := make([]byte, 32*1024)
	// Fetch a string builder
	buf := stringsbuilder.New()
	defer stringsbuilder.Reuse(buf)
	// Start reading
	var err error
	for err == nil {
		var n int
		select {
		case <-doneC:
			err = context.Canceled
		default:
			n, err = r.Read(b)
		}
		buf.Write(b[:n])
	}
	// Handle any errors
	if err != io.EOF || buf.Len() == 0 {
		return "", err
	}
	// Return the string
	return buf.String(), nil
}

// ReaderArg turns a continuation argument into an io.Reader.
func ReaderArg(c *rt.GoCont, n int) (io.Reader, error) {
	r, ok := ValueToReader(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an io.Reader", n+1)
	}
	return r, nil
}

// ValueToReader turns a Lua value to an io.Reader, if possible.
func ValueToReader(v rt.Value) (io.Reader, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	r, ok := u.Value().(io.Reader)
	return r, ok
}

// ReadCloserArg turns a continuation argument into an io.ReadCloser.
func ReadCloserArg(c *rt.GoCont, n int) (io.ReadCloser, error) {
	r, ok := ValueToReadCloser(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an io.ReadCloser", n+1)
	}
	return r, nil
}

// ValueToReadCloser turns a Lua value to an io.ReadCloser, if possible.
func ValueToReadCloser(v rt.Value) (io.ReadCloser, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	r, ok := u.Value().(io.ReadCloser)
	return r, ok
}
