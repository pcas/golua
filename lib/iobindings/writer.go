// Writer contains reusable Lua bindings for an io.Writer.

package iobindings

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"fmt"
	"io"
)

// Flusher is the interface satisfied by the Flush method.
type Flusher interface {
	Flush() error
}

// Syncer is the interface satisfied by the Sync method.
type Syncer interface {
	Sync() error
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// Flush TODO
//
// Note that Flush will fallback to calling the Sync method if the argument does not support Flusher but does support Syncer.
func Flush(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	} else if w, ok := ValueToFlusher(c.Arg(0)); ok {
		return FlushWithFlusher(w, c)
	} else if w, ok := ValueToSyncer(c.Arg(0)); ok {
		return SyncWithSyncer(w, c)
	}
	return nil, errors.New("#1 must describe an object with either a Flush or a Sync method")
}

// FlushWithFlusher TODO
func FlushWithFlusher(w Flusher, c rt.Cont) (rt.Cont, error) {
	return PushingNextResult(c, rt.Bool(true), w.Flush())
}

// SyncWithSyncer TODO
func SyncWithSyncer(w Syncer, c rt.Cont) (rt.Cont, error) {
	return PushingNextResult(c, rt.Bool(true), w.Sync())
}

// Write TODO
func Write(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	} else if _, err := WriterArg(c, 0); err != nil {
		return nil, err
	}
	return WriteWithWriter(c.Arg(0), c)
}

// WriteWithWriter TODO
func WriteWithWriter(v rt.Value, c *rt.GoCont) (rt.Cont, error) {
	w, ok := ValueToWriter(v)
	if !ok {
		return nil, errors.New("argument must describe an io.Writer")
	}
	for _, val := range c.Etc() {
		switch val.(type) {
		case rt.String:
		case rt.Int:
		case rt.Float:
		default:
			return nil, errors.New("argument must be a string or a number")
		}
		s, _ := rt.AsString(val)
		if _, err := io.WriteString(w, string(s)); err != nil {
			return PushingError(c, err)
		}
	}
	return c.PushingNext(v), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// WriteValues writes the given values to w.
func WriteValues(w io.Writer, vals []rt.Value) error {
	for _, val := range vals {
		switch val.(type) {
		case rt.String:
		case rt.Int:
		case rt.Float:
		default:
			return errors.New("argument must be a string or a number")
		}
		s, _ := rt.AsString(val)
		if _, err := io.WriteString(w, string(s)); err != nil {
			return err
		}
	}
	return nil
}

// WriterArg turns a continuation argument into an io.Writer.
func WriterArg(c *rt.GoCont, n int) (io.Writer, error) {
	w, ok := ValueToWriter(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an io.Writer", n+1)
	}
	return w, nil
}

// ValueToWriter turns a Lua value to an io.Writer, if possible.
func ValueToWriter(v rt.Value) (io.Writer, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	w, ok := u.Value().(io.Writer)
	return w, ok
}

// WriteCloserArg turns a continuation argument into an io.WriteCloser.
func WriteCloserArg(c *rt.GoCont, n int) (io.WriteCloser, error) {
	w, ok := ValueToWriteCloser(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an io.WriteCloser", n+1)
	}
	return w, nil
}

// ValueToWriteCloser turns a Lua value to an io.WriteCloser, if possible.
func ValueToWriteCloser(v rt.Value) (io.WriteCloser, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	w, ok := u.Value().(io.WriteCloser)
	return w, ok
}

// ReadWriterArg turns a continuation argument into an io.ReadWriter.
func ReadWriterArg(c *rt.GoCont, n int) (io.ReadWriter, error) {
	rw, ok := ValueToReadWriter(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an io.ReadWriter", n+1)
	}
	return rw, nil
}

// ValueToReadWriter turns a Lua value to an io.ReadWriter, if possible.
func ValueToReadWriter(v rt.Value) (io.ReadWriter, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	rw, ok := u.Value().(io.ReadWriter)
	return rw, ok
}

// ReadWriteCloserArg turns a continuation argument into an io.ReadWriteCloser.
func ReadWriteCloserArg(c *rt.GoCont, n int) (io.ReadWriteCloser, error) {
	rw, ok := ValueToReadWriteCloser(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an io.ReadWriteCloser", n+1)
	}
	return rw, nil
}

// ValueToReadWriteCloser turns a Lua value to an io.ReadWriteCloser, if possible.
func ValueToReadWriteCloser(v rt.Value) (io.ReadWriteCloser, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	rw, ok := u.Value().(io.ReadWriteCloser)
	return rw, ok
}

// FlusherArg turns a continuation argument into a Flusher.
func FlusherArg(c *rt.GoCont, n int) (Flusher, error) {
	w, ok := ValueToFlusher(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an object with a Flush method", n+1)
	}
	return w, nil
}

// ValueToFlusher turns a Lua value to a Flusher, if possible.
func ValueToFlusher(v rt.Value) (Flusher, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	w, ok := u.Value().(Flusher)
	return w, ok
}

// SyncerArg turns a continuation argument into a Syncer.
func SyncerArg(c *rt.GoCont, n int) (Syncer, error) {
	w, ok := ValueToSyncer(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an object with a Sync method", n+1)
	}
	return w, nil
}

// ValueToSyncer turns a Lua value to a Syncer, if possible.
func ValueToSyncer(v rt.Value) (Syncer, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	w, ok := u.Value().(Syncer)
	return w, ok
}
