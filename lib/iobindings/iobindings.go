// Iobindings contains reusable Lua bindings for io operations.

package iobindings

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"syscall"
	"time"
)

// causer is the interface satisfied by an error with a Cause method.
type causer interface {
	Cause() error
}

// IsCloseder is the interface satisfied by the IsClosed method.
type IsCloseder interface {
	IsClosed() bool
}

// SetTimeouter is the interface satisfied by the SetTimeout method.
type SetTimeouter interface {
	SetTimeout(time.Duration)
}

// GetTimeouter is the interface satisfied by the GetTimeout method.
type GetTimeouter interface {
	GetTimeout() time.Duration
}

// NilTimeout is the nil-valued timeout.
const NilTimeout = time.Duration(-1)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// errno TODO
func errno(err error) (syscall.Errno, bool) {
	for err != nil {
		switch e := err.(type) {
		case syscall.Errno:
			return e, true
		case *os.LinkError:
			err = e.Err
		case *os.PathError:
			err = e.Err
		case *os.SyscallError:
			err = e.Err
		case *net.OpError:
			err = e.Err
		case causer:
			cause := e.Cause()
			if cause == err {
				return 0, false
			}
			err = cause
		default:
			return 0, false
		}
	}
	return 0, false
}

// toTimeout attempts to convert the given value to a timeout. The second return value will be true iff the value could be converted.
func toTimeout(v rt.Value) (time.Duration, bool) {
	// Get the nil case out of the way
	if rt.IsNil(v) {
		return NilTimeout, true
	}
	// Is v an integer?
	if n, ok := rt.ToInt(v); ok {
		if n < 0 {
			return NilTimeout, true
		}
		return time.Duration(n) * time.Second, true
	}
	// Is v a float?
	if f, ok := rt.ToFloat(v); ok {
		if f < 0 {
			return NilTimeout, true
		}
		return time.Duration(float64(f) * float64(time.Second)), true
	}
	// No good
	return 0, false
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// Seek TODO
func Seek(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := SeekerArg(c, 0)
	if err != nil {
		return nil, err
	}
	// Extract the seek behaviour
	whence := io.SeekCurrent
	offset := int64(0)
	nargs := c.NArgs()
	if nargs >= 2 {
		whenceName, err := c.StringArg(1)
		if err != nil {
			return nil, err
		}
		switch whenceName {
		case "cur":
			whence = io.SeekCurrent
		case "set":
			whence = io.SeekStart
		case "end":
			whence = io.SeekEnd
		default:
			return nil, errors.New("#2 must be 'cur', 'set' or 'end'")
		}
	}
	if nargs >= 3 {
		offsetI, err := c.IntArg(2)
		if err != nil {
			return nil, err
		}
		offset = int64(offsetI)
	}
	// Perform the seek
	pos, ioErr := s.Seek(offset, whence)
	return PushingNextResult(c, rt.Int(pos), ioErr)
}

// Close TODO
func Close(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	cl, err := CloserArg(c, 0)
	if err != nil {
		return nil, err
	}
	return CloseWithCloser(cl, c)
}

// CloseWithCloser TODO
func CloseWithCloser(cl io.Closer, c rt.Cont) (rt.Cont, error) {
	return PushingNextResult(c, rt.Bool(true), cl.Close())
}

// IsClosed TODO
func IsClosed(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	cl, err := IsClosederArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Bool(cl.IsClosed())), nil
}

// SetTimeout sets the timeout, defining a limit on the amount of time the I/O methods can block. When a timeout is set and the specified amount of time has elapsed, the affected methods give up and fail with an error message. The amount of time to wait is specified as the value parameter, in seconds.
//
// The nil timeout value allows operations to block indefinitely. Negative timeout values have the same effect.
// Args: settimeouter [value]
func SetTimeout(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the SetTimeouter
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := SetTimeouterArg(c, 0)
	if err != nil {
		return nil, err
	}
	// Recover the timeout
	timeout := NilTimeout
	if c.NArgs() > 1 {
		var ok bool
		timeout, ok = toTimeout(c.Arg(1))
		if !ok {
			return nil, errors.New("#2 must be nil or a number")
		}
	}
	s.SetTimeout(timeout)
	return c.PushingNext(rt.Bool(true)), nil
}

// SetTimeoutWithSetTimeouter sets the timeout, defining a limit on the amount of time the I/O methods can block. When a timeout is set and the specified amount of time has elapsed, the affected methods give up and fail with an error message. The amount of time to wait is specified as the value parameter, in seconds.
//
// The nil timeout value allows operations to block indefinitely. Negative timeout values have the same effect.
// Args: [value]
func SetTimeoutWithSetTimeouter(s SetTimeouter, c *rt.GoCont) (rt.Cont, error) {
	// Recover the timeout
	timeout := NilTimeout
	if c.NArgs() > 0 {
		var ok bool
		timeout, ok = toTimeout(c.Arg(1))
		if !ok {
			return nil, errors.New("#1 must be nil or a number")
		}
	}
	s.SetTimeout(timeout)
	return c.PushingNext(rt.Bool(true)), nil
}

// GetTimeout returns the current I/O timeout, in seconds. A nil timeout value allows operations to block indefinitely.
// Args: gettimeouter
func GetTimeout(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := GetTimeouterArg(c, 0)
	if err != nil {
		return nil, err
	}
	return GetTimeoutWithGetTimeouter(s, c)
}

// GetTimeoutWithGetTimeouter returns the current I/O timeout, in seconds. A nil timeout value allows operations to block indefinitely.
// Args:
func GetTimeoutWithGetTimeouter(s GetTimeouter, c rt.Cont) (rt.Cont, error) {
	next := c.Next()
	if timeout := s.GetTimeout(); timeout < 0 {
		next.Push(nil)
	} else {
		next.Push(rt.Float(float64(timeout) / float64(time.Second)))
	}
	return next, nil
}

// ToString TODO
func ToString(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := StringerArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(s.String())), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// PushingNextResult TODO
func PushingNextResult(c rt.Cont, v rt.Value, err error) (rt.Cont, error) {
	if err != nil {
		return PushingError(c, err)
	}
	next := c.Next()
	next.Push(v)
	return next, nil
}

// PushingError TODO
func PushingError(c rt.Cont, err error) (rt.Cont, error) {
	next := c.Next()
	if err == context.DeadlineExceeded {
		next.Push(nil)
		next.Push(rt.String("timeout"))
	} else if err == context.Canceled {
		return nil, errors.New("user interrupt")
	} else {
		next.Push(nil)
		if err != nil {
			next.Push(rt.String(err.Error()))
			if n, ok := errno(err); ok {
				next.Push(rt.Int(n))
			}
		}
	}
	return next, nil
}

// SeekerArg turns a continuation argument into an io.Seeker.
func SeekerArg(c *rt.GoCont, n int) (io.Seeker, error) {
	s, ok := ValueToSeeker(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an io.Seeker", n+1)
	}
	return s, nil
}

// ValueToSeeker turns a Lua value to an io.Seeker, if possible.
func ValueToSeeker(v rt.Value) (io.Seeker, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	s, ok := u.Value().(io.Seeker)
	return s, ok
}

// CloserArg turns a continuation argument into an io.Closer.
func CloserArg(c *rt.GoCont, n int) (io.Closer, error) {
	cl, ok := ValueToCloser(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an io.Closer", n+1)
	}
	return cl, nil
}

// ValueToCloser turns a Lua value to an io.Closer, if possible.
func ValueToCloser(v rt.Value) (io.Closer, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	cl, ok := u.Value().(io.Closer)
	return cl, ok
}

// IsClosederArg turns a continuation argument into an IsCloseder.
func IsClosederArg(c *rt.GoCont, n int) (IsCloseder, error) {
	cl, ok := ValueToIsCloseder(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an object with an IsClosed method", n+1)
	}
	return cl, nil
}

// ValueToIsCloseder turns a Lua value to an IsCloseder, if possible.
func ValueToIsCloseder(v rt.Value) (IsCloseder, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	cl, ok := u.Value().(IsCloseder)
	return cl, ok
}

// SetTimeouterArg turns a continuation argument into a SetTimeouter.
func SetTimeouterArg(c *rt.GoCont, n int) (SetTimeouter, error) {
	s, ok := ValueToSetTimeouter(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an object with a SetTimeout method", n+1)
	}
	return s, nil
}

// ValueToSetTimeouter turns a Lua value to a SetTimeouter, if possible.
func ValueToSetTimeouter(v rt.Value) (SetTimeouter, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	s, ok := u.Value().(SetTimeouter)
	return s, ok
}

// GetTimeouterArg turns a continuation argument into a GetTimeouter.
func GetTimeouterArg(c *rt.GoCont, n int) (GetTimeouter, error) {
	s, ok := ValueToGetTimeouter(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe an object with a GetTimeout method", n+1)
	}
	return s, nil
}

// ValueToGetTimeouter turns a Lua value to a GetTimeouter, if possible.
func ValueToGetTimeouter(v rt.Value) (GetTimeouter, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	s, ok := u.Value().(GetTimeouter)
	return s, ok
}

// StringerArg turns a continuation argument into a fmt.Stringer.
func StringerArg(c *rt.GoCont, n int) (fmt.Stringer, error) {
	s, ok := ValueToStringer(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must describe a fmt.Stringer", n+1)
	}
	return s, nil
}

// ValueToStringer turns a Lua value to a fmt.Stringer, if possible.
func ValueToStringer(v rt.Value) (fmt.Stringer, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	s, ok := u.Value().(fmt.Stringer)
	return s, ok
}
