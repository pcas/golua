package packagelib

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

const (
	pkgKey       = rt.String("package")
	preloadKey   = rt.String("preload")
	pathKey      = rt.String("path")
	configKey    = rt.String("config")
	loadedKey    = rt.String("loaded")
	searchersKey = rt.String("searchers")
	defaultPath  = `./?.lua;./?/init.lua`
)

type config struct {
	dirSep                 string
	pathSep                string
	placeholder            string
	windowsExecPlaceholder string
	suffixSep              string
}

// init registers this module.
func init() {
	module.Register(module.Info{
		Name:        "package",
		Description: "Tools for loading additional packages.",
		URL:         "https://www.lua.org/manual/5.3/manual.html#6.3",
		Preload:     true,
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			env := r.GlobalEnv()
			pkg := rt.NewTable()
			pkg.Set(loadedKey, rt.NewTable())
			pkg.Set(preloadKey, rt.NewTable())
			searchers := rt.NewTable()
			searchers.Set(rt.Int(1), searchPreloadGoFunc)
			searchers.Set(rt.Int(2), searchLuaGoFunc)
			pkg.Set(searchersKey, searchers)
			pkg.Set(pathKey, rt.String(defaultPath))
			pkg.Set(configKey, rt.String(defaultConfig.String()))
			rt.SetEnvGoFunc(pkg, "searchpath", searchpath, 4, false)
			rt.SetEnvGoFunc(env, "require", require, 1, false)
			return pkg, nil
		},
	})
}

func (c *config) String() string {
	return fmt.Sprintf("%s\n%s\n%s\n%s\n%s",
		c.dirSep, c.pathSep, c.placeholder,
		c.windowsExecPlaceholder, c.suffixSep)
}

var defaultConfig = config{"/", ";", "?", "!", "-"}

func getConfig(pkg *rt.Table) *config {
	conf := new(config)
	*conf = defaultConfig
	confStr, ok := pkg.Get(configKey).(rt.String)
	if !ok {
		return conf
	}
	lines := strings.Split(string(confStr), "\n")
	if len(lines) >= 1 {
		conf.dirSep = lines[0]
	}
	if len(lines) >= 2 {
		conf.pathSep = lines[1]
	}
	if len(lines) >= 3 {
		conf.placeholder = lines[2]
	}
	if len(lines) >= 4 {
		conf.windowsExecPlaceholder = lines[3]
	}
	if len(lines) >= 5 {
		conf.suffixSep = lines[4]
	}
	return conf
}

func require(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	modname, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	mod, err := Require(string(modname), t)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(mod), nil
}

// Require loads the given module. The function starts by looking into the package.loaded table to determine whether modname is already loaded. If it is, then require returns the value stored at package.loaded[modname]. Otherwise, it tries to find a loader for the module. See the documentation for the Lua "require" function for details.
func Require(modname string, t *rt.Thread) (rt.Value, error) {
	pkg := pkgTable(t)
	// First check is the module is already loaded...
	loaded, ok := pkg.Get(loadedKey).(*rt.Table)
	if !ok {
		return nil, errors.New("package.loaded must be a table")
	} else if mod := loaded.Get(rt.String(modname)); !rt.IsNil(mod) {
		return mod, nil
	}
	// ...no luck, so start working through the searchers array
	searchers, ok := pkg.Get(searchersKey).(*rt.Table)
	if !ok {
		return nil, errors.New("package.searchers must be a table")
	}
	for i := 1; ; i++ {
		// Fetch the next searcher
		searcher := searchers.Get(rt.Int(i))
		if rt.IsNil(searcher) {
			return nil, fmt.Errorf("could not find package '%s'", modname)
		}
		// Call the searcher with the package modname
		res := rt.NewTerminationWith(2, false)
		if err := rt.Call(t, searcher, []rt.Value{rt.String(modname)}, res); err != nil {
			return nil, err
		}
		// If got a loader, call it
		if loader, ok := res.Get(0).(rt.Callable); ok {
			val := res.Get(1)
			res = rt.NewTerminationWith(2, false)
			if err := rt.Call(t, loader, []rt.Value{rt.String(modname), val}, res); err != nil {
				return nil, err
			}
			var mod rt.Value = rt.Bool(true)
			if r0 := res.Get(0); !rt.IsNil(r0) {
				mod = r0
			}
			rt.SetEnv(loaded, modname, mod)
			return mod, nil
		}
	}
}

func searchpath(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	var name, path rt.String
	sep := rt.String(".")
	conf := *getConfig(pkgTable(t))
	rep := rt.String(conf.dirSep)

	err := c.CheckNArgs(2)
	if err == nil {
		name, err = c.StringArg(0)
	}
	if err == nil {
		path, err = c.StringArg(1)
	}
	if err == nil && c.NArgs() >= 3 {
		sep, err = c.StringArg(2)
	}
	if err == nil && c.NArgs() >= 4 {
		rep, err = c.StringArg(3)
	}
	if err != nil {
		return nil, err
	}
	conf.dirSep = string(rep)
	found, templates := searchPath(string(name), string(path), string(sep), &conf)
	next := c.Next()
	if found != "" {
		next.Push(rt.String(found))
	} else {
		rt.Push(next, nil, rt.String("tried: "+strings.Join(templates, "\n")))
	}
	return next, nil
}

func searchPath(name, path, dot string, conf *config) (string, []string) {
	namePath := strings.Replace(name, dot, conf.dirSep, -1)
	templates := strings.Split(path, conf.pathSep)
	for i, template := range templates {
		searchpath := strings.Replace(template, conf.placeholder, namePath, -1)
		f, err := os.Open(searchpath)
		f.Close()
		if err == nil {
			return searchpath, nil
		}
		templates[i] = searchpath
	}
	return "", templates
}

func searchPreload(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	loader := pkgTable(t).Get(preloadKey).(*rt.Table).Get(s)
	c.Next().Push(loader)
	return c.Next(), nil
}

func searchLua(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the package name
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	next := c.Next()
	// Perhaps this is a registered module?
	if info, ok := module.GetInfo(string(s)); ok {
		next.Push(rt.NewGoFunction(func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
			// Load any dependencies
			for _, name := range info.Requires {
				if _, err := Require(name, t); err != nil {
					return nil, fmt.Errorf("error loading package '%s' (unable to load dependency): %w", info.Name, err)
				}
			}
			// Load the module
			m, err := info.Load(t.Runtime)
			if err != nil {
				return nil, fmt.Errorf("error loading package '%s': %w", info.Name, err)
			}
			next := c.Next()
			next.Push(m)
			return next, nil

		}, info.Name, 0, false))
		return next, nil
	}
	// Recover the paths
	pkg := pkgTable(t)
	path, ok := pkg.Get(pathKey).(rt.String)
	if !ok {
		return nil, errors.New("package.path must be a string")
	}
	// Search the paths
	conf := getConfig(pkg)
	found, templates := searchPath(string(s), string(path), ".", conf)
	if found == "" {
		next.Push(rt.String(strings.Join(templates, "\n")))
	} else {
		next.Push(loadLuaGoFunc)
		next.Push(rt.String(found))
	}
	return next, nil
}

var loadLuaGoFunc = rt.NewGoFunction(loadLua, "loadlua", 2, false)
var searchLuaGoFunc = rt.NewGoFunction(searchLua, "searchlua", 1, false)
var searchPreloadGoFunc = rt.NewGoFunction(searchPreload, "searchpreload", 1, false)

func loadLua(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Arg 0 is the module name - dunno what to do with it.
	filePath, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	src, readErr := ioutil.ReadFile(string(filePath))
	if readErr != nil {
		return nil, fmt.Errorf("error reading file: %w", readErr)
	}
	clos, compErr := rt.CompileAndLoadLuaChunk(string(filePath), src, t.GlobalEnv())
	if compErr != nil {
		return nil, fmt.Errorf("error compiling file: %w", compErr)
	}
	return rt.Continue(t, clos, c.Next())
}

func pkgTable(t *rt.Thread) *rt.Table {
	return t.Registry(pkgKey).(*rt.Table)
}

// LoadedTable returns the package.loaded table.
func LoadedTable(r *rt.Runtime) (*rt.Table, error) {
	pkg := r.Registry(pkgKey).(*rt.Table)
	loaded, ok := pkg.Get(loadedKey).(*rt.Table)
	if !ok {
		return nil, errors.New("package.loaded must be a table")
	}
	return loaded, nil
}

// CachedLoadedTable returns the package.loaded.name table.
func CachedLoadedTable(r *rt.Runtime, name rt.String) (*rt.Table, error) {
	// Recover the package.loaded table
	loaded, err := LoadedTable(r)
	if err != nil {
		return nil, err
	}
	// Recover the package.loaded.name table
	tab, ok := loaded.Get(name).(*rt.Table)
	if !ok {
		return nil, fmt.Errorf("package.loaded.%s must be a table", name)
	}
	return tab, nil
}

// CallPackageFunc calls the function "modname.funcname" with the given arguments. The module modname will be loaded into the runtime via require.
func CallPackageFunc(t *rt.Thread, modname string, funcname string, arg ...rt.Value) ([]rt.Value, error) {
	// Ensure that the module is loaded
	m, err := Require(modname, t)
	if err != nil {
		return nil, err
	}
	mod, ok := m.(*rt.Table)
	if !ok {
		return nil, fmt.Errorf("package '%s' must be a table", modname)
	}
	// Recover the function
	vf := mod.Get(rt.String(funcname))
	if rt.IsNil(vf) {
		return nil, fmt.Errorf("'%s.%s' not defined", modname, funcname)
	}
	f, ok := vf.(rt.Callable)
	if !ok {
		return nil, fmt.Errorf("'%s.%s' is not callable", modname, funcname)
	}
	// Call the function
	res := rt.NewTerminationWith(0, true)
	if err := rt.Call(t, f, arg, res); err != nil {
		return nil, err
	}
	// Return the results
	return res.Etc(), nil
}
