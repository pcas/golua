package packagelib_test

import (
	"bitbucket.org/pcas/golua/luatesting"
	"testing"
)

func TestPackageLib(t *testing.T) {
	luatesting.RunLuaTestsInDir(t, "lua", nil)
}
