// Module provides the concept of a module when working with the Lua vm.

package module

import (
	"bitbucket.org/pcas/golua/lib/base"
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"fmt"
	"sort"
	"sync"
)

// LoadFunc is a function that loads and returns a module.
type LoadFunc func(*rt.Runtime) (*rt.Table, error)

// Info is used to register a module.
type Info struct {
	Name        string   // The module name
	Description string   // A brief description of the module
	URL         string   // A url for further documentation
	Load        LoadFunc // The function that creates and returns the module
	Preload     bool     // Is the module pre-loaded?
	Requires    []string // Ensure that the given modules are loaded first. Note that circular requirements are not protected against.
}

// The map of modules that have been registered, along with a controlling mutex.
var (
	registerMutex = &sync.Mutex{}
	register      = make(map[string]Info)
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Init installs the preloaded modules onto the runtime.
func Init(r *rt.Runtime) error {
	// Begin by installing the base functions
	if err := base.Load(r); err != nil {
		return fmt.Errorf("error loading package 'base': %w", err)
	}
	// Make a note of the global env
	env := r.GlobalEnv()
	// We need to load the package module next
	var loaded *rt.Table
	if info, ok := GetInfo("package"); !ok {
		return errors.New("unable to locate package 'package'")
	} else {
		// Sanity check -- the package module cannot have any requirements
		if len(info.Requires) != 0 {
			return errors.New("package 'package' must not have dependencies")
		}
		// Load the module
		m, err := info.Load(r)
		if err != nil {
			return fmt.Errorf("error loading package 'package': %w", err)
		}
		r.SetRegistry(rt.String(info.Name), m)
		// Extract the loaded table
		if loaded, ok = m.Get(rt.String("loaded")).(*rt.Table); !ok {
			return errors.New("package.loaded must be a table")
		}
		// Install the module as a global variable and cache it
		rt.SetEnv(env, info.Name, m)
		rt.SetEnv(loaded, info.Name, m)
	}
	// Install the modules marked as preloaded
	for _, info := range Registered() {
		if info.Preload && info.Name != "package" {
			// Sanity check -- preloaded modules cannot have any requirements
			if len(info.Requires) != 0 {
				return fmt.Errorf("package '%s' must not have dependencies", info.Name)
			}
			// Load the module
			m, err := info.Load(r)
			if err != nil {
				return fmt.Errorf("error loading package '%s': %w", info.Name, err)
			}
			// Install the module as a global variable and cache it
			rt.SetEnv(env, info.Name, m)
			rt.SetEnv(loaded, info.Name, m)
		}
	}
	return nil
}

// Register registers the given module to be installed in any future vm. This will panic if the provided information is invalid, or if a module is already registered with this name.
func Register(info Info) {
	// Sanity check
	if len(info.Name) == 0 {
		panic("Illegal empty module name")
	} else if info.Load == nil {
		panic("Illegal nil module Load function: " + info.Name)
	}
	// Acquire a lock on the register of modules
	registerMutex.Lock()
	defer registerMutex.Unlock()
	// Is this name already registered? If not, register it
	if _, ok := register[info.Name]; ok {
		panic("Module already registered: " + info.Name)
	}
	register[info.Name] = info
}

// IsRegistered returns true iff the named module is registered.
func IsRegistered(name string) bool {
	registerMutex.Lock()
	defer registerMutex.Unlock()
	_, ok := register[name]
	return ok
}

// GetInfo returns returns the Info for the given module. The second return value will be false iff the module is not registered.
func GetInfo(name string) (Info, bool) {
	registerMutex.Lock()
	defer registerMutex.Unlock()
	info, ok := register[name]
	return info, ok
}

// Registered returns a slice of of all currently registered modules.
func Registered() []Info {
	registerMutex.Lock()
	defer registerMutex.Unlock()
	S := make([]Info, 0, len(register))
	for _, info := range register {
		S = append(S, info)
	}
	sort.Slice(S, func(i int, j int) bool { return S[i].Name < S[j].Name })
	return S
}
